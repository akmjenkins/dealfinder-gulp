<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>


<div class="body">

	<section>
		<div class="sw">
		
			<div class="hgroup">
				<h1 class="hgroup-title">Pro V Series</h1>
			</div><!-- .hgroup -->
			
			<p class="excerpt">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br /> Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus.
			</p><!-- .excerpt -->
		
			<div class="single-item grid pad40 collapse-950">
			
				<div class="col col-2">
					<div class="single-item-thumb item">
						<div class="lazybg with-img">
							<img src="../assets/dist/images/temp/featured-boat.jpg" alt="featured boat">
						</div><!-- .lazybg -->
					</div><!-- .single-item-thumb -->
				</div><!-- .col -->
			
				<div class="col col-2 item">
					<div class="single-item-content article-body">
					
						<div class="hgroup">
							<h3 class="single-item-title hgroup-title">$12,987</h3>
							<span class="single-item-subtitle">PRO V-Series</span>
						</div><!-- .hgroup -->
					
						<div class="single-item-meta">
							<a href="#" class="button small fill">Share</a>
							<a href="#" class="button small fill">Print</a>
						</div><!-- .single-item-meta -->
						
						<p>
							Cras ut pellentesque magna, eu rhoncus dolor. Integer vulputate tortor vel nisi euismod, eget bibendum turpis tempus. 
							Nulla dictum enim a sem cursus fermentum. Praesent diam arcu, mollis quis rutrum et, rhoncus sit amet dui. Interdum 
							et malesuada fames ac ante ipsum primis in faucibus. Aenean est est, vulputate sed mauris eget, vulputate gravida 
							eros. Nulla in diam ornare justo vehicula feugiat quis vel purus. Cras interdum erat sit amet nisl ultrices, ut rutrum turpis 
							imperdiet. Nullam eget tellus a justo ornare ornare. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse 
							tincidunt odio quis ligula auctor, eget laoreet nisi tempus. Nulla et purus sit amet nibh maximus venenatis. Pellentesque
							sed quam felis. Proin sed semper risus. Morbi sit amet nunc at dui pulvinar auctor.
						</p>
					
					</div><!-- .single-item-content -->
				</div><!-- .col -->
				
				
			</div><!-- .single-item -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="lighter-secondary-bg">
		<div class="sw">
		
			<div class="request-information grid collapse-950">
				<div class="col col-2-5">
					<div class="request-information-info item d-bg">
						<h4>Request Information</h4>
						<p>
							Suspendisse tincidunt odio quis ligula auctor, eget laoreet nisi
							tempus. Nulla et purus sit amet nibh maximus venenatis.
							Pellentesque sed quam felis. Proin sed semper risus. Morbi sit
							amet nunc at dui pulvinar auctor. Suspendisse tincidunt odio 
							quis ligula auctor, eget laoreet nisi tempus. Nulla et purus sit 
							amet nibh maximus venenatis. Pellentesque sed quam 
							felis. Proin sed semper risus. Morbi sit amet nunc at dui 
							pulvinar auctor.
						</p>
					</div><!-- .request-information-info -->
				</div><!-- .col -->
			
				<div class="col col-3-5">
					<div class="request-information-form item">
						<form action="/" class="body-form full">
							<div class="grid eqh collapse-700">
							
								<div class="col col-2">
									<div class="item fieldset">
										<span class="field-wrap"><input type="text" name="fname" placeholder="First Name"></span>
										<span class="field-wrap"><input type="text" name="lname" placeholder="Last Name"></span>
										<span class="field-wrap"><input type="tel" name="phone" placeholder="Last Name"></span>
										<span class="field-wrap"><input type="email" name="email" placeholder="E-mail"></span>
									</div>
								</div><!-- .fieldset -->
								
								<div class="col col-2">
									<textarea class="item" name="comments" placeholder="Comments" cols="30" rows="10"></textarea>
								</div><!-- .fieldset -->
								
								<div class="col col-1">
									<div class="item">
										<label class="block">
											<input type="checkbox" name="offers">
											E-mail me info on future offers
										</label>
										
										<button class="button primary fill">Submit</button>		
									</div><!-- .item -->
								</div><!-- .col -->
								
							</div><!-- .fieldset-wrap -->
							
						</form><!-- .body-form -->
					</div><!-- .request-information-form -->
				</div><!-- .col -->
			</div><!-- .request-information -->
		
		</div><!-- .sw -->
	</section><!-- .d-bg -->

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>