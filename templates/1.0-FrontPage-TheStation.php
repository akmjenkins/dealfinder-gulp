<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	
	<div class="hero-content">
		<div class="sw">
		
			<h1 class="hero-title">We Have What You're Looking For, and More!</h1>
			
			<div class="hero-form-wrap">
				<form action="/" class="single-form">
					<div class="fieldset">
						<input name="s" type="text" placeholder="What are you looking for?">
						<button class="t-fa-abs fa-search">Search</button>
					</div><!-- .fieldset -->
				</form><!-- .single-form -->
				<a href="#">More Search Options</a>
			</div><!-- .hero-form-wrap -->
		
		</div><!-- .sw -->
	</div><!-- .hero-content -->
	
	<div class="hero-swiper swiper-wrapper">
		<div class="swiper"
			data-arrows="false" 
			data-autoplay="true"
			data-autoplay-speed="7000"
			data-pause-on-hover="false"
			data-update-lazy-images="true" 
			data-dots="true"
			data-fade="true">
			
			<!-- data-fade="detect" will make this a touch swiper on touch devices, and a fader on non-touch devices -->
		
			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/dist/images/temp/hero/hero-1.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>
			</div><!-- .swipe-item -->

			<div class="swipe-item">
				<div class="swipe-item-bg" data-src="../assets/dist/images/temp/hero/hero-2.jpg,http://dummyimage.com/1200x500/000/fff 1200w,http://dummyimage.com/600x500/000/fff 600w,"></div>			
			</div><!-- .swipe-item -->
			
		</div><!-- .swiper -->
		
	</div><!-- .swiper-wrapper -->
	
</div><!-- .hero -->

<div class="body">


	<section>
		<div class="sw">
		
			<div class="hgroup">
				<h2 class="hgroup-title">Categories</h2>
			</div><!-- .hgroup -->
			
			<div class="grid cat-items eqh">
			
				<div class="col-4 sm-col-2 col">
					<a class="item cat-item cat-item-blue bounce" href="#">
						<span class="cat-item-ico lazybg img" data-src="../assets/dist/images/vectors/boat.svg"></span>
						<span class="cat-item-title">Boats</span>
					</a><!-- .item -->
				</div><!-- .col -->
				
				<div class="col-4 sm-col-2 col">
					<a class="item cat-item cat-item-green bounce" href="#">
						<span class="cat-item-ico lazybg img" data-src="../assets/dist/images/vectors/rv.svg"></span>
						<span class="cat-item-title">RV's</span>
					</a><!-- .item -->
				</div><!-- .col -->
				
				<div class="col-4 sm-col-2 col">
					<a class="item cat-item cat-item-red bounce" href="#">
						<span class="cat-item-ico lazybg img" data-src="../assets/dist/images/vectors/heavy-equipment.svg"></span>
						<span class="cat-item-title">Heavy Equipment</span>
					</a><!-- .item -->
				</div><!-- .col -->
				
				<div class="col-4 sm-col-2 col">
					<a class="item cat-item cat-item-grey bounce" href="#">
						<span class="cat-item-ico lazybg img" data-src="../assets/dist/images/vectors/box.svg"></span>
						<span class="cat-item-title">Other</span>
					</a><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .grid -->
			
		</div><!-- .sw -->
	</section>
	
	<section>
		<div class="sw">
		
			<div class="hgroup">
				<h2 class="hgroup-title">Featured Listings</h2>
			</div><!-- .hgroup -->
			
			<div class="grid eqh blocks collapse-at-850 blocks">
				
				<div class="lg-col-4 col md-col-2">
					<div class="item featured-item">
					
						<a class="block with-button keep-img " href="#">
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/boat.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style featured-item-title">Speed Boat</span>
								</div><!-- .hgroup -->
								
								<span class="h5-style featured-item-head">Description</span>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
								
								<span class="button full outline">View Details</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="lg-col-4 col md-col-2">
					<div class="item featured-item">
					
						<a class="block with-button keep-img " href="#">
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/rv.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style featured-item-title">RV 45,000km</span>
								</div><!-- .hgroup -->
								
								<span class="h5-style featured-item-head">Description</span>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida</p>
								
								<span class="button full outline">View Details</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="lg-col-4 col md-col-2">
					<div class="item featured-item">
					
						<a class="block with-button keep-img " href="#">
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/backhoe.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style featured-item-title">Backhoe</span>
								</div><!-- .hgroup -->
								
								<span class="h5-style featured-item-head">Description</span>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
								
								<span class="button full outline">View Details</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="lg-col-4 col md-col-2">
					<div class="item featured-item">
					
						<a class="block with-button keep-img " href="#">
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/motor.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style featured-item-title">2003 Sea Hunt 21'</span>
								</div><!-- .hgroup -->
								
								<span class="h5-style featured-item-head">Description</span>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
								
								<span class="button full outline">View Details</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .grid -->
			
			<div class="center">
				<a href="#" class="button fill">View All Listings</a>
			</div><!-- .center -->
			
		</div><!-- .sw -->
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>