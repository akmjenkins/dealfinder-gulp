			<footer class="dark-bg">
			
				<div class="sw">
					<div class="footer-row">
					
						<div class="footer-logo">
							<a href="/" class="lazybg with-img" data-src="../assets/dist/images/deal-finder-nl-logo-light.svg"></a>
						</div><!-- .footer-logo -->
					
						<div class="footer-contact">
						
							<h6>Contact Us</h6>
						
							<address>
								Newfoundland and Labrador Classifieds Inc. <br />
								1037 Topsail Road <br />
								Mount Pearl, NL, A1N 5E9
							</address>
							
							<span class="block">Tel: (709) 237.7300</span>
							<span class="block">Fax: (709) 237.7300</span>
							
							<?php include('i-social.php'); ?>
						</div><!-- .footer-contact -->
						
						<div class="footer-nav">
						
							<h6>Main Links</h6>
							
							<ul>
								<li><a href="#">Boats</a></li>
								<li><a href="#">RV's</a></li>
								<li><a href="#">Heavy Equipment</a></li>
								<li><a href="#">Other</a></li>
								<li><a href="#">Financing</a></li>
								<li><a href="#">The Latest</a></li>
								<li><a href="#">Resources</a></li>
								<li><a href="#">Suppliers</a></li>
								<li><a href="#">Contact</a></li>
							</ul>
							
						</div><!-- .footer-nav -->
						
						<div class="footer-info">
						
							<h6>About All Seasons Recreation</h6>
							
							<p>
								Sed dictum sem ac hendrerit elementum. Maecenas aliquet ante id tortor bibendum egestas. In eu consectetur augue, ut rutrum dolor. 
								Fusce non sagittis ipsum. Integer vel vehicula sapien, sed dapibus eros. Ut id massa lacinia, vestibulum urna quis, condimentum mi. 
								Aliquam erat volutpat. Quisque convallis nunc sit amet eros dignissim, auctor aliquam nisl congue. Nullam id ornare nisi.
							</p>
							
							<a href="#" class="button primary fill">Read More</a>
						
						</div><!-- .footer-info -->
					
					</div><!-- .footer-row -->
				</div><!-- .sw -->
				
				<div class="copyright">
					<div class="sw">
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">All Season Recreation</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/dist/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
				
			</footer><!-- .footer -->

		</div><!-- .page-wrapper -->
		
		<form action="/" method="get" class="global-search-form">
			<div class="fieldset">
				<input type="search" name="s" placeholder="Search DealFinderNL">
				<span class="close toggle-search">×</span>
			</div>
		</form><!-- .global-search-form -->		

		<script>
			var templateJS = {
				templateURL: 'http://dev.me/station',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>

		<script src="../assets/dist/js/main.gulp.js"></script>
	</body>
</html>