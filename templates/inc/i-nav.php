<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>

<div class="nav">
	<div class="sw full">
		
		<nav>
		
			<ul>
				<li class="with-drop">				
					<a href="#">Boats</a>
					
					<div>				
						<ul>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/river-hawk-boats.jpg"></span>
									<span>River Hawk Boats and more</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/trailers.jpg"></span>
									<span>Trailers</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/new-boats.jpg"></span>
									<span>New Boats</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/engines.jpg"></span>
									<span>Engines</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/preowned-boats.jpg"></span>
									<span>Pre-Owned Boats</span>
								</a>
							</li>
						</ul>
					</div>
				</li>
				<li class="with-drop">
					<a href="#">RV's</a>
					
					<div>				
						<ul>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/river-hawk-boats.jpg"></span>
									<span>River Hawk Boats and more</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/trailers.jpg"></span>
									<span>Trailers</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/new-boats.jpg"></span>
									<span>New Boats</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/engines.jpg"></span>
									<span>Engines</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/preowned-boats.jpg"></span>
									<span>Pre-Owned Boats</span>
								</a>
							</li>
						</ul>
					</div>

					
				</li>
				<li class="with-drop">
					<a href="#">Heavy Equipment</a>
					
					<div>				
						<ul>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/river-hawk-boats.jpg"></span>
									<span>River Hawk Boats and more</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/trailers.jpg"></span>
									<span>Trailers</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/new-boats.jpg"></span>
									<span>New Boats</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/engines.jpg"></span>
									<span>Engines</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/preowned-boats.jpg"></span>
									<span>Pre-Owned Boats</span>
								</a>
							</li>
						</ul>
					</div>

				</li>
				<li class="with-drop">
					<a href="#">Other</a>
					<div>				
						<ul>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/river-hawk-boats.jpg"></span>
									<span>River Hawk Boats and more</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/trailers.jpg"></span>
									<span>Trailers</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/new-boats.jpg"></span>
									<span>New Boats</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/engines.jpg"></span>
									<span>Engines</span>
								</a>
							</li>
							<li>
								<a href="#">
									<span class="lazybg" data-src="../assets/dist/images/temp/nav/preowned-boats.jpg"></span>
									<span>Pre-Owned Boats</span>
								</a>
							</li>
						</ul>
					</div>

				</li>
				<li><a href="#">Financing</a></li>
				<li><a href="#">The Latest</a></li>
			</ul>
			
			<button class="t-fa-abs fa-search toggle-search">Search</button>
		</nav>
		
		<div class="nav-meta">
			<ul>
				<li><a href="#">Resources</a></li>
				<li><a href="#">Supplies</a></li>
				<li><a href="#">Contact</a></li>
			</ul>
		</div><!-- .nav-meta -->
		
	</div><!-- .sw -->
	
	<form action="/" class="nav-search-form single-form">
		<div class="fieldset sw">
			<input type="text" name="s" placeholder="Search DealFinder NL...">
			<button class="t-fa-abs fa-search">Search</button>
			<button class="t-fa-abs fa-close toggle-search" type="button">Close</button>
		</div><!-- .fieldset -->
	</form><!-- .search-form -->
	
</div><!-- .nav -->