<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section class="dark-bg secondary-bg contact-map-wrap">
		<div class="contact-map"></div>
	
		<div class="sw">
		
			<div class="hgroup">
				<h1 class="hgroup-title">Contact</h1>
			</div><!-- .hgroup -->
			
			<p class="excerpt">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br /> Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus.
			</p><!-- .excerpt -->
			
			<div class="contact-grid">
				<div class="contact-grid-form">
					<form action="/" class="body-form full">
						<div class="fieldset grid pad10 collapse-650">
							<div class="col col-1">
								<div class="item"><input type="text" name="name" placeholder="Full Name"></div>
							</div><!-- .col -->
							<div class="col col-1">
								<div class="item"><input type="email" name="email" placeholder="Email"></div>
							</div><!-- .col -->
							<div class="col col-1">
								<div class="item"><input type="text" name="address" placeholder="Address"></div>
							</div><!-- .col -->
							<div class="col col-3-5">
								<div class="item"><input type="text" name="city" placeholder="City"></div>
							</div><!-- .col -->
							<div class="col col-1-5">
								<div class="item">
									<div class="selector with-arrow">
										<select name="province">
											<option value="">Province</option>
											<option value="AB" data-tag="AB">Alberta</option>
											<option value="BC" data-tag="BC">British Columbia</option>
											<option value="MB" data-tag="MB">Manitoba</option>
											<option value="NB" data-tag="NB">New Brunswick</option>
											<option value="NL" data-tag="NL">Newfoundland and Labrador</option>
											<option value="NS" data-tag="NS">Nova Scotia</option>
											<option value="ON" data-tag="ON">Ontario</option>
											<option value="PE" data-tag="PE">Prince Edward Island</option>
											<option value="QC" data-tag="QC">Quebec</option>
											<option value="SK" data-tag="SK">Saskatchewan</option>
											<option value="NT" data-tag="NT">Northwest Territories</option>
											<option value="NU" data-tag="NU">Nunavut</option>
											<option value="YT" data-tag="YT">Yukon</option>
										</select>
										<span class="value">&nbsp;</span>
									</div>
								</div>
							</div><!-- .col -->
							<div class="col col-1-5">
								<div class="item"><input type="text" name="postal" placeholder="Postal" maxlength="6"></div>
							</div><!-- .col -->
							<div class="col col-1">
								<div class="item">
									<textarea name="comments" placeholder="Comments" cols="30" rows="10"></textarea>
								</div><!-- .item -->
							</div><!-- .col -->
							<div class="col col-1">
								<div class="item">
									<button class="button primary fill">Submit</button>
								</div><!-- .item -->
							</div><!-- .col -->
						</div><!-- .grid -->
					</form><!-- .body-form -->
				</div><!-- .contact-grid-form -->
				
				<div class="contact-info">
				
					<div class="lazybg ib with-img map-marker-placeholder">
						<img src="../assets/dist/images/map-marker.png" alt="map marker">
					</div><!-- .lazybg -->
					
					<address>
						1037 Topsail Road <br />
						Mount Pearl, NL, A1N 5E9
					</address>
					
					<span class="block">Tel: (709) 237.7300</span>
					<span class="block">Business Hours: 9am - 5pm Monday to Friday</span>
						<br />
					<span class="block">Telephone Hours: 9am - 5pm  and 7pm - 11pm Everyday</span>
				
				</div><!-- .contact-info -->

			</div><!-- .contact-grid -->
			
			
		</div><!-- .sw -->
	</section>

	<section class="d-bg lighter-secondary-bg">
		<div class="sw">
			<?php include('inc/i-testimonial.php'); ?>
		</div>
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>