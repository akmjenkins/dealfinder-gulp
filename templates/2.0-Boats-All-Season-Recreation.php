<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
			<div class="hgroup">
				<h1 class="hgroup-title">Boats</h1>
			</div><!-- .hgroup -->
			
			<p class="excerpt">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br /> Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus.
			</p><!-- .excerpt -->
						
			<div class="ov-grid grid nopad eqh">
			
				<div class="col">
					<a href="#" class="item ov-item d-bg bounce">
						<span class="ov-item-bg lazybg img" data-src="../assets/dist/images/temp/blocks/block-1.jpg"></span>
					
						<div class="hgroup">
							<h4 class="hgroup-title">River Hawk Boats</h4>
						</div><!-- .hgroup -->
						
						<div class="ov-hover">
							<p>
								Lorem ipsum dolor sit amet, consectetur
								adipiscing elit. Aenean euismod bibendum
								laoreet. Proin gravida dolor sit amet lacus.
							</p>
							
							<span class="button outline">View</span>
						</div><!-- .hover -->
						
					</a><!-- .ov-item -->
				</div><!-- .col -->
				
				<div class="col">
					<a href="#" class="item ov-item d-bg bounce">
						<span class="ov-item-bg lazybg img" data-src="../assets/dist/images/temp/blocks/block-2.jpg"></span>
						<div class="hgroup">
							<h4 class="hgroup-title">New Boats</h4>
						</div><!-- .hgroup -->
						
						<div class="ov-hover">
							<p>
								Lorem ipsum dolor sit amet, consectetur
								adipiscing elit. Aenean euismod bibendum
								laoreet. Proin gravida dolor sit amet lacus.
							</p>
							
							<span class="button outline">View</span>
						</div><!-- .hover -->
						
					</a><!-- .ov-item -->
				</div><!-- .col -->
				
				<div class="col">
					<a href="#" class="item ov-item d-bg bounce">
						<span class="ov-item-bg lazybg img" data-src="../assets/dist/images/temp/blocks/block-3.jpg"></span>
						<div class="hgroup">
							<h4 class="hgroup-title">Pre-Owned Boats</h4>
						</div><!-- .hgroup -->
						
						<div class="ov-hover">
							<p>
								Lorem ipsum dolor sit amet, consectetur
								adipiscing elit. Aenean euismod bibendum
								laoreet. Proin gravida dolor sit amet lacus.
							</p>
							
							<span class="button outline">View</span>
						</div><!-- .hover -->
						
					</a><!-- .ov-item -->
				</div><!-- .col -->
				
				<div class="col">
					<a href="#" class="item ov-item d-bg bounce">
						<span class="ov-item-bg lazybg img" data-src="../assets/dist/images/temp/blocks/block-4.jpg"></span>
						<div class="hgroup">
							<h4 class="hgroup-title">Trailers</h4>
						</div><!-- .hgroup -->
						
						<div class="ov-hover">
							<p>
								Lorem ipsum dolor sit amet, consectetur
								adipiscing elit. Aenean euismod bibendum
								laoreet. Proin gravida dolor sit amet lacus.
							</p>
							
							<span class="button outline">View</span>
						</div><!-- .hover -->
						
					</a><!-- .ov-item -->
				</div><!-- .col -->
				
				<div class="col">
					<a href="#" class="item ov-item d-bg bounce">
						<span class="ov-item-bg lazybg img" data-src="../assets/dist/images/temp/blocks/block-5.jpg"></span>
						<div class="hgroup">
							<h4 class="hgroup-title">Engines</h4>
						</div><!-- .hgroup -->
						
						<div class="ov-hover">
							<p>
								Lorem ipsum dolor sit amet, consectetur
								adipiscing elit. Aenean euismod bibendum
								laoreet. Proin gravida dolor sit amet lacus.
							</p>
							
							<span class="button outline">View</span>
						</div><!-- .hover -->
						
					</a><!-- .ov-item -->
				</div><!-- .col -->
				
				<div class="col">
					<a href="#" class="item ov-item d-bg bounce">
						<span class="ov-item-bg lazybg img" data-src="../assets/dist/images/temp/blocks/block-1.jpg"></span>
					
						<div class="hgroup">
							<h4 class="hgroup-title">Resources</h4>
						</div><!-- .hgroup -->
						
						<div class="ov-hover">
							<p>
								Lorem ipsum dolor sit amet, consectetur
								adipiscing elit. Aenean euismod bibendum
								laoreet. Proin gravida dolor sit amet lacus.
							</p>
							
							<span class="button outline">View</span>
						</div><!-- .hover -->
						
					</a><!-- .ov-item -->
				</div><!-- .col -->
				
			</div><!-- .ov-grid -->
			
		</div><!-- .sw -->
	</section>
	
	<section class="d-bg lighter-secondary-bg">
		<div class="sw">
			<?php include('inc/i-testimonial.php'); ?>
		</div>
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>