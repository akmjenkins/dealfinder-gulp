<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>


<div class="body">

	<section>
		<div class="sw">
		
			<div class="hgroup">
				<h1 class="hgroup-title">River Hawk Boats</h1>
			</div><!-- .hgroup -->
			
			<p class="excerpt">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br /> Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus.
			</p><!-- .excerpt -->
			
			<div class="grid eqh blocks collapse-at-850 blocks">

				<div class="lg-col-4 col md-col-2">
					<div class="item featured-item">
					
						<a class="block with-button keep-img " href="#">
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/boat-1.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style featured-item-title">River Hawk 2015</span>
								</div><!-- .hgroup -->
								
								<span class="h5-style featured-item-head">Description</span>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
								
								<span class="button full outline">View Details</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="lg-col-4 col md-col-2">
					<div class="item featured-item">
					
						<a class="block with-button keep-img " href="#">
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/boat-2.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style featured-item-title">River Hawk 2012</span>
								</div><!-- .hgroup -->
								
								<span class="h5-style featured-item-head">Description</span>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida</p>
								
								<span class="button full outline">View Details</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="lg-col-4 col md-col-2">
					<div class="item featured-item">
					
						<a class="block with-button keep-img " href="#">
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/boat-3.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style featured-item-title">River Hawk 2011</span>
								</div><!-- .hgroup -->
								
								<span class="h5-style featured-item-head">Description</span>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
								
								<span class="button full outline">View Details</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="lg-col-4 col md-col-2">
					<div class="item featured-item">
					
						<a class="block with-button keep-img " href="#">
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/boat-4.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style featured-item-title">River Hawk 2010</span>
								</div><!-- .hgroup -->
								
								<span class="h5-style featured-item-head">Description</span>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
								
								<span class="button full outline">View Details</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
			
				<div class="lg-col-4 col md-col-2">
					<div class="item featured-item">
					
						<a class="block with-button keep-img " href="#">
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/boat-1.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style featured-item-title">River Hawk 2015</span>
								</div><!-- .hgroup -->
								
								<span class="h5-style featured-item-head">Description</span>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
								
								<span class="button full outline">View Details</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="lg-col-4 col md-col-2">
					<div class="item featured-item">
					
						<a class="block with-button keep-img " href="#">
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/boat-2.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style featured-item-title">River Hawk 2012</span>
								</div><!-- .hgroup -->
								
								<span class="h5-style featured-item-head">Description</span>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida</p>
								
								<span class="button full outline">View Details</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="lg-col-4 col md-col-2">
					<div class="item featured-item">
					
						<a class="block with-button keep-img " href="#">
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/boat-3.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style featured-item-title">River Hawk 2011</span>
								</div><!-- .hgroup -->
								
								<span class="h5-style featured-item-head">Description</span>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
								
								<span class="button full outline">View Details</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->

				<div class="lg-col-4 col md-col-2">
					<div class="item featured-item">
					
						<a class="block with-button keep-img " href="#">
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/boat-4.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style featured-item-title">River Hawk 2010</span>
								</div><!-- .hgroup -->
								
								<span class="h5-style featured-item-head">Description</span>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
								
								<span class="button full outline">View Details</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .grid -->
			
		</div><!-- .sw -->
	</section>
	
	<section class="d-bg lighter-secondary-bg">
		<div class="sw">
			<?php include('inc/i-testimonial.php'); ?>
		</div>
	</section>

</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>