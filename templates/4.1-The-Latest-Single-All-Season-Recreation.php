<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
		<div class="sw">
		
			<div class="hgroup">
				<h1 class="hgroup-title">The Latest - Single</h1>
			</div><!-- .hgroup -->
			
			<p class="excerpt">
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br /> Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus.
			</p><!-- .excerpt -->
			
			<div class="main-body">				
				<div class="content">

					<div class="article-list">
					
						<div class="article-list-item article-body">
							<article>
							
								<div class="featured-img lazybg with-img">
									<img src="../assets/dist/images/temp/featured.jpg" alt="featured image">
								</div><!-- .featured-img -->
							
								<time class="article-time" datetime="2015-04-25" pubdate>April 25, 2015</time>
								<h1 class="article-title"><a href="#">News title. Mauris dui nulla, accumsan in variusconsectetur mauris.</a></h1>
								
								<p class="article-excerpt">
									Ea qui noster oporteat similique, cu duo audiam mentitum intellegebat. Eum eius probo ancillae te, mei eu diam
									populo ridens. Cu vim choro pertinax deseruisse. Mei ea electram argumentum, modus legere eu vim. Ea qui
									noster oporteat similique, cu duo audiam mentitum intellegebat. Eum eius probo ancillae te, mei eu diam
									populo ridens. Cu vim choro pertinax deseruisse. Mei ea electram argumentum, modus legere eu vim.
								</p>
								
							</article>
						</div><!-- .article-list-item -->
						
					</div><!-- .article-list -->
					
				</div><!-- .content -->
				<aside class="sidebar">
				
					<div class="sidebar-mod acc-mod">
						<h5 class="mod-title">News Archive</h5>

						<div class="acc with-indicators">

							<div class="acc-item">
								<div class="acc-item-handle">
									2015 
								</div>
								<div class="acc-item-content">
									
									<ul>
										<li><a href="#">January (5)</a></li>
										<li><a href="#">February (5)</a></li>
										<li><a href="#">March (3)</a></li>
									</ul>

								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->
							
							<div class="acc-item">
								<div class="acc-item-handle">
									2014
								</div>
								<div class="acc-item-content">
									
									<ul>
										<li><a href="#">January (5)</a></li>
										<li><a href="#">February (5)</a></li>
										<li><a href="#">March (3)</a></li>
									</ul>

								</div><!-- .acc-item-content -->
							</div><!-- .acc-item -->

						</div><!-- .acc -->
					</div><!-- .sidebar-mod -->

				</aside><!-- .sidebar -->
			</div><!-- .main-body -->
			
		</div><!-- .sw -->
	</section>

	<section class="d-bg lighter-secondary-bg">
		<div class="sw">
			<?php include('inc/i-testimonial.php'); ?>
		</div>
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>