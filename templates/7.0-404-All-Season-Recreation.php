<?php $bodyclass = 'error404'; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<div class="error404-body lazybg" data-src="../assets/dist/images/vectors/404.svg">
		<div class="sw">

			<span class="error404-top">
				404. Something went wrong. <br />
				Like whatever happened to that guys boat.
			</span>
			
			<span class="error404-bottom">
				If only he had a <a href="#link" class="inline">Riverhawk</a> boat.
			</span>
		
		</div><!-- .sw -->
	</div><!-- .error404-body -->
	
	<section class="d-bg lighter-secondary-bg">
		<div class="sw">
			<?php include('inc/i-testimonial.php'); ?>
		</div>
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>